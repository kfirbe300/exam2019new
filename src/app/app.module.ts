import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';

import { LoginComponent } from './login/login.component';

import { environment } from '../environments/environment';

import {MatCardModule} from '@angular/material/card';

import { Routes, RouterModule } from '@angular/router';

//Firebase modules 
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';

import {MatButtonModule} from '@angular/material/button';
import { RegisterComponent } from './register/register.component';
import { BooksComponent } from './books/books.component';
import { BookComponent } from './book/book.component'; 

import {MatCheckboxModule} from '@angular/material/checkbox';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    RegisterComponent,
    BooksComponent,
    BookComponent,
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    AngularFireModule ,
    AngularFireDatabaseModule ,
    AngularFireAuthModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    FormsModule,
    MatButtonModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule.forRoot([
      { path:'', component: BooksComponent },
      { path:'books', component: BooksComponent },
      { path:'book', component: BookComponent },
      { path:'login', component: LoginComponent },
      { path:'register', component: RegisterComponent },
      { path:'**', component: LoginComponent}
    ])

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
