import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';


@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  password: string;
  name: string;
  confirmpassword: string;
  nickname:string;
  
  error = ''; //תופס את השגיאות
  output = '';//הדפסת שגיאה למשתמש
  required = '';//הדפסת הערה שדה חובה
  require = true; //דגל עבור שדה חובה- אם הכל מלא טרו
  flag = false; //דגל עבור שגיאה (6 תווים בסיסמה וכו)- אם אין פולס
  specialChar = ['!','@','#','$','%','^','&','*','(',')','_','-','+','=']; //תווים נדרשים-דרושים תווים מיוחדים
  valid = false; //דגל עמידה בסיסמה- אם לא עומדים בכל התנאים פולס
  validation = ""; //הודעה לתווים מיוחדים
  equal = true;
 

  register() // כשלוחצים על כפתור
  {
    this.flag = false;//אין שגיאה
    this.valid = false;//עמידה בסיסמה, לא עומדים
    this.require = true; //כל השדות מלאים

    if (this.name == null || this.password == null || this.email == null || this.confirmpassword == null || this.nickname == null )  //אם השם ריק או הסיסמה איקה או האימייל ריק- שלב 1
    {
      this.required = "this input is required";//תדפיס שדה נדרש
      this.require = false;//סימון שהשדו הנדרשים לא הודפסו
    }

    if (!(this.confirmpassword == this.password))
    {
      this.equal = false;
    }

    if (this.confirmpassword == this.password)
    {
      this.equal = true;
    }

    if(this.require)//אם כל השדות מלאים-שלב 2
    {
      for (let char of this.specialChar) //עבור כל התווים שציינו
      {
        if (this.password.includes(char))//מספיק שהסיסמה מכילה אחד מהם
        {
          this.valid = true;//ווהיא ולד והכל טוב
        }
      }
    }
    if (this.valid && this.require && this.equal) // אם הסיסמה וליד וגם השדות לא ריקים-שלב 3
    {//הרשמה
      this.authService.register(this.email,this.password)
      .then(value => { 
        this.authService.updateProfile(value.user, this.name);
        this.authService.addUser(value.user,this.name, this.email,this.nickname);
      }).then(value => {
        this.router.navigate(['/books']);
      }).catch(err => { //אפ צצה איזשהי שגיאה
        this.flag = true; //תסמן שיש שגיאה
        this.error = err.code;//נשמור את קוד השגיאה
        this.output = err.message; //ואת ההודעה של השגיאה- שתודפס אם יש שגיאה
      })
    }
    else //הודעה על תווים נדרשים
    {
      this.validation = "Password must contain special characters";
    }
  }

  constructor(public authService:AuthService, private router:Router, public afAuth: AngularFireAuth) { }

  ngOnInit() {
   
  }

  onLoginGoogle() //הרשמה גוגל
  {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    this.router.navigate(['/todos']);
  }



}